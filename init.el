;; improve startup time by pausing garbage collection during init
(setq gc-cons-threshold most-positive-fixnum)

(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/") t)
(package-initialize)

(setq package-selected-packages
      '(which-key
	magit
	solarized-theme
	multiple-cursors
	tree-sitter
	tree-sitter-langs
	))

(when (cl-find-if-not #'package-installed-p package-selected-packages)
  (package-refresh-contents)
  (mapc #'package-install package-selected-packages))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(column-number-mode t)
 '(custom-enabled-themes '(solarized-dark-high-contrast))
 '(custom-safe-themes
   '("51ec7bfa54adf5fff5d466248ea6431097f5a18224788d0bd7eb1257a4f7b773" "3e200d49451ec4b8baa068c989e7fba2a97646091fd555eca0ee5a1386d56077" "f5b6be56c9de9fd8bdd42e0c05fecb002dedb8f48a5f00e769370e4517dde0e8" "4c56af497ddf0e30f65a7232a8ee21b3d62a8c332c6b268c81e9ea99b11da0d3" "00445e6f15d31e9afaa23ed0d765850e9cd5e929be5e8e63b114a3346236c44c" "285d1bf306091644fb49993341e0ad8bafe57130d9981b680c1dbd974475c5c7" "7f1d414afda803f3244c6fb4c2c64bea44dac040ed3731ec9d75275b9e831fe5" "833ddce3314a4e28411edf3c6efde468f6f2616fc31e17a62587d6a9255f4633" "830877f4aab227556548dc0a28bf395d0abe0e3a0ab95455731c9ea5ab5fe4e1" "fee7287586b17efbfda432f05539b58e86e059e78006ce9237b8732fde991b4c" default))
 '(delete-by-moving-to-trash t)
 '(global-display-line-numbers-mode t)
 '(global-hl-line-mode t)
 '(package-selected-packages
   '(cmake-mode lua-mode rust-mode nasm-mode eglot-java java-mode julia-mode php-mode jupyter impatient-mode yaml-mode typescript-mode zig-mode glsl-mode which-key magit solarized-theme multiple-cursors))
 '(reb-re-syntax 'string)
 '(ring-bell-function 'ignore)
 '(tab-bar-mode t)
 '(tab-width 4)
 '(tool-bar-mode nil)
 '(truncate-lines t))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Source Code Pro" :foundry "ADBO" :slant normal :weight regular :height 120 :width normal))))
 '(mode-line-highlight ((t (:box (:line-width (2 . 2) :color "grey40" :style released-button))))))

;;;;;;;;;;;;;;;;
;; show-paren ;;
;;;;;;;;;;;;;;;;
(show-paren-mode t)

;;;;;;;;;;;;;;;
;; which key ;;
;;;;;;;;;;;;;;;
(which-key-mode)

;;;;;;;;;;;;;
;; compile ;;
;;;;;;;;;;;;;
(global-set-key (kbd "C-c c") 'compile)
(global-set-key (kbd "C-c r") 'recompile)

(defun colorize-compilation-buffer ()
  (ansi-color-apply-on-region compilation-filter-start (point-max)))
(add-hook 'compilation-filter-hook 'colorize-compilation-buffer)

;;;;;;;;;;;;;;;;;
;; comment box ;;
;;;;;;;;;;;;;;;;;
(global-set-key (kbd "C-x C-'") 'comment-box)

;;;;;;;;;;;;;;;;;;;;;;
;; multiple cursors ;;
;;;;;;;;;;;;;;;;;;;;;;
(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
(global-set-key (kbd "C->")         'mc/mark-next-like-this)
(global-set-key (kbd "C-<")         'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<")     'mc/mark-all-like-this)
(global-set-key (kbd "C-\"")        'mc/skip-to-next-like-this)
(global-set-key (kbd "C-:")         'mc/skip-to-previous-like-this)

;;;;;;;;;;;;;;;;
;; dired copy ;;
;;;;;;;;;;;;;;;;
(setq dired-dwim-target t)

;;;;;;;;;;;;;;;;;
;; tree-sitter ;;
;;;;;;;;;;;;;;;;;
(global-tree-sitter-mode)

(setq backup-directory-alist `(("." . "~/.saves")))
(setq column-number-mode t)

;; ??
(add-hook 'after-init-hook
	  (lambda ()
	    (find-file "~/.emacs.d/init.el")))

(put 'upcase-region 'disabled nil)

;; jump to error in compile window
(setq compilation-skip-threshold 2)

;; c3
(setq treesit-language-source-alist
      '((c3 "https://github.com/c3lang/tree-sitter-c3")))
(add-to-list 'load-path "~/opt/c3/c3-ts-mode")
(require 'c3-ts-mode)
